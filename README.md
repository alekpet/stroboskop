# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://alekpet@bitbucket.org/alekpet/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/alekpet/stroboskop/commits/52738cf5730d7255556edb973f25a13afa9112a5

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/alekpet/stroboskop/commits/16bf75b206ea5c2924125607085da2cd2fab48c7

Naloga 6.3.2:
https://bitbucket.org/alekpet/stroboskop/commits/7f3b98e1dadbed0a4f2a8f0d23c964899b75c994

Naloga 6.3.3:
https://bitbucket.org/alekpet/stroboskop/commits/083bf1ffa68119721072f3a402fc22d5a3402f86

Naloga 6.3.4:
https://bitbucket.org/alekpet/stroboskop/commits/b284b1fe8c572d9e717d97485b232dbb499e6899

Naloga 6.3.5:

```
git merge master izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/alekpet/stroboskop/commits/e16da59864d96251996b7b1c2f1be0642cdb74c3

Naloga 6.4.2:
https://bitbucket.org/alekpet/stroboskop/commits/8142fd56fdb6b9a2c10a754276d4c90cb3da855e

Naloga 6.4.3:
https://bitbucket.org/alekpet/stroboskop/commits/9fc71ff010f97be8b5f4d54650f22958c22272d7

Naloga 6.4.4:
https://bitbucket.org/alekpet/stroboskop/commits/411bd71748d2f0960c5f3acfea55fcfb06270b8e